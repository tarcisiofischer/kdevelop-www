---
title: Get KDevelop
hideMeta: true
author: Tarcisio Fischer
date: 2022-05-14T14:40:05+00:00
sassFiles:
  - /scss/get-it.scss
menu:
  main:
    weight: 3
---

<b>[You can download the source code from here (all platforms)](https://download.kde.org/stable/kdevelop/5.6.1/src/)</b>

{{< get-it src="/images/Tux.svg_-254x300.png" >}}

### Linux distributions

+  [Run the KDevelop 5.6.1 (64bit) AppImage](https://download.kde.org/stable/kdevelop/5.6.1/bin/linux/KDevelop-5.6.1-x86_64.AppImage) (verify file by: [GPG signature](https://download.kde.org/stable/kdevelop/5.6.1/bin/linux/KDevelop-5.6.1-x86_64.AppImage.sig), key linked below).
Just enter the following commands in a terminal to download & start KDevelop:

```
wget -O KDevelop.AppImage https://download.kde.org/stable/kdevelop/5.6.1/bin/linux/KDevelop-5.6.1-x86_64.AppImage
chmod +x KDevelop.AppImage
./KDevelop.AppImage
```

<small>
Note: Your system libraries or preferences are not altered. In case you'd like to remove KDevelop again, simply delete the downloaded file.
Learn more about AppImage here.
</small>

+  or [install KDevelop from your Linux distribution](https://userbase.kde.org/KDevelop/Install#GNU.2FLinux)

+  or [build KDevelop from source](https://community.kde.org/KDevelop/HowToCompile_v5)

{{< /get-it >}}

{{< get-it src="/images/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows (Maintainers welcome!)

No official pre-built installers available currently

+  [Run the experimental KDevelop 5.5.0 (64bit) installer for Windows 7 or newer](http://download.kde.org/stable/kdevelop/5.5.0/bin/windows/kdevelop-5.5.0-x64-setup.exe)

<small>Note that we don't ship a C/C++ compiler. Instructions on how to set up a compiler and build system for C++ development can be found [here](https://userbase.kde.org/KDevelop4/Manual/WindowsSetup).</small>

+  or [install KDevelop via Chocolatey](https://chocolatey.org/packages/kdevelop/):

```
choco install kdevelop
```

+ or [build KDevelop from source](https://userbase.kde.org/KDevelop/Install#Microsoft_Windows)

{{< /get-it >}}

{{< get-it src="/images/macOS-logo-2017.png" >}}

### macOS (Preview Versions)

No official pre-built installers available yet

+ [Try to run the experimental KDevelop (64bit) DMG from KDE's "Binary Factory"](https://binary-factory.kde.org/job/KDevelop_Release_macos/lastSuccessfulBuild/artifact/) (maintainers needed)
+ or [build KDevelop from source](https://community.kde.org/Mac)

{{< /get-it >}}

{{< get-it-info >}}

**Latest releases** <br>

+ [KDevelop 5.6.1 (current stable branch)](https://www.kdevelop.org/news/kdevelop-561-released) | Dec 08, 2020 (Qt 5.8, KF5 5.50, or higher)
+ [KDevelop 5.5.2 (old stable branch)](https://www.kdevelop.org/news/kdevelop-552-released) | Jun 2, 2020 (Qt 5.7, KF5 5.28, or higher)
+ [KDevelop 4.7.4 (4.x branch)](https://www.kdevelop.org/news/kdevelop-474-released) | Dec 13, 2016 (kdelibs 4.7, or higher)


**Nightly builds**

KDE's "Binary Factory" builds Windows installers on a nightly basis. (Wanted: someone to maintain the same for AppImages and macOS DMG files)

[Download KDevelop (64-bit) Windows installer, 5.5 branch, nightly build](https://binary-factory.kde.org/view/Management/job/KDevelop_Stable_win64/lastSuccessfulBuild/artifact/)


**Legacy releases**

[See the list of all release announcements](https://www.kdevelop.org/taxonomy/term/28) or [browse the archive](https://download.kde.org/stable/kdevelop/).


**Feedback on KDevelop binaries/installers**

If you're experiencing problems with the binaries or installers, please either [report a bug](https://bugs.kde.org/enter_bug.cgi?product=kdevelop&format=guided) or [send us a mail](https://www.kdevelop.org/contact/kdevelop_feedback).


**Signatures**

Binaries and sources are signed with any of those GPG keys:

+ Sven Brauch, key fingerprint 329F D02C 5AA4 8FCC 77A4 BBF0 AC44 AC6D B297 79E6
+ Kevin Funk, key fingerprint 364E FA5F 6395 7290 7D39 2999 C64C F56B 13CA CE5D, [key file (.asc)](http://kfunk.org/shared/files/kfunk.asc)
+ Friedrich W. H. Kossebau, key fingerprint E191 FD5B E6F4 6870 F09E 82B2 024E 7FB4 3D01 5474, [key file (.asc)](https://share.kde.org/s/nM3SxBYFww2fPdT)
+ Friedrich W. H. Kossebau, key fingerprint 0A48 BC96 1075 B4BA 8523 E379 0A34 5FB0 86E7 97D9, [key file (.asc)](https://share.kde.org/s/8dKaJTw8pR8iiwJ)
+ Milian Wolff, key fingerprint C51B 45A5 32F1 7FA4 01D0 99A0 A0C6 B72C 4F1C 5E7C, [key file (.asc)](https://share.kde.org/s/jnMxL2ZB9FEXLow)
+ [GPG block of all above combined: kdevelop.gpg](https://share.kde.org/s/XNaxTtBd6PEzqmp)

{{< /get-it-info >}}
