---
title: 'KDevelop - A cross-platform IDE for C, C++, Python, QML/JavaScript and PHP'
name: KDevelop
subtitle: "A cross-platform IDE for C, C++, Python, QML/JavaScript and PHP"
menu:
  main:
    name: KDevelop
    weight: 1
---
