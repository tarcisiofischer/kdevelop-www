# kdevelop.org

This repository contains the full kdevelop.org website.

## Local development

1. Install 'go' `sudo pacman install go`
2. Install 'hugo' with specific version `extended_0.94.0` (Please note the `extended`) from https://github.com/gohugoio/hugo/releases/tag/v0.94.2
3. From one level above this repository, clone the shared theme using `git clone git@invent.kde.org:websites/aether-sass.git -b hugo --single-branch`
4. Run `./server.sh`

